import arcade

SCREEN_WIDTH = 1024
SCREEN_HEIGHT = 720
SCREEN_TITLE = "Discovering the magic of Machine Learning"
SPRITES_URI = "resources/images/"
MUSIC_VOLUME = 0.01


class InstructionView(arcade.View):
    def on_show(self):
        arcade.set_background_color(arcade.csscolor.DARK_SLATE_BLUE)

    def on_draw(self):
        arcade.start_render()
        arcade.draw_text("Instructions Screen", SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2,
                         arcade.color.WHITE, font_size=50, anchor_x="center")
        arcade.draw_text("Click to advance", SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2 - 75,
                         arcade.color.WHITE, font_size=20, anchor_x="center")

    def on_mouse_press(self, _x, _y, _button, _modifiers):
        """ If the user presses the mouse button, start the game. """
        game_view = MyGameWindow(SCREEN_WIDTH,SCREEN_HEIGHT,SCREEN_TITLE)
        self.window.show_view(game_view)


class MyGameWindow(arcade.View):
    def __init__(self, width, height, title):
        super().__init__()
        # self.set_location(400, 200)
        self.bouncing_fig1_x = 100
        self.bouncing_fig1_y = 100
        self.bouncing_fig1_x_speed = 300
        self.bouncing_fig1_y_speed = 100
        self.player_x = 200
        self.player_y = 200
        self.player_speed = 250
        self.up = False
        self.right = False
        self.left = False
        self.down = False

        # Sprites
        self.sprite1 = arcade.Sprite(SPRITES_URI + "ml-sprite-small.png", center_x=69.5, center_y=64)
        self.sprite_list = arcade.SpriteList()
        self.sprite_list.append(self.sprite1)
        self.sprite_x = 500
        self.sprite_y = 500
        self.sprite_speed = 250
        self.s_up = False
        self.s_right = False
        self.s_left = False
        self.s_down = False

        # background
        self.background = arcade.load_texture(":resources:images/backgrounds/abstract_1.jpg")
        arcade.set_background_color(arcade.color.AMAZON)

        # music
        self.music_song = arcade.Sound("resources/music/1918.mp3", streaming=True)
        self.music_song.play(volume=MUSIC_VOLUME)

    def on_draw(self):
        arcade.start_render()
        # background
        #arcade.draw_lrwh_rectangle_textured(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, self.background)
        arcade.draw_line(0,0,500,500, arcade.color.AZURE, 5)
        arcade.draw_circle_filled(self.bouncing_fig1_x, self.bouncing_fig1_y, 50, arcade.csscolor.BEIGE, 20)
        arcade.draw_circle_outline(self.player_x, self.player_y, 50, arcade.csscolor.DARK_RED)
        self.sprite_list.draw()


    def on_update(self, delta_time):
        self.bouncing_fig1_x += self.bouncing_fig1_x_speed * delta_time
        self.bouncing_fig1_y += self.bouncing_fig1_y_speed * delta_time
        if self.bouncing_fig1_x > SCREEN_WIDTH or self.bouncing_fig1_x < 0:
            self.bouncing_fig1_x_speed *= -1
        if self.bouncing_fig1_y > SCREEN_HEIGHT or self.bouncing_fig1_y < 0:
            self.bouncing_fig1_y_speed *= -1
        if self.right:
            self.player_x += self.player_speed * delta_time
        if self.left:
            self.player_x -= self.player_speed * delta_time
        if self.up:
            self.player_y += self.player_speed * delta_time
        if self.down:
            self.player_y -= self.player_speed * delta_time
        if self.s_right:
            self.sprite_x += self.sprite_speed * delta_time
        if self.s_left:
            self.sprite_x -= self.sprite_speed * delta_time
        if self.s_up:
            self.sprite_y += self.sprite_speed * delta_time
        if self.s_down:
            self.sprite_y -= self.sprite_speed * delta_time

        # for the sprite:
        self.sprite1.set_position(self.sprite_x, self.sprite_y)

    def on_key_press(self, symbol, modifiers):
        if symbol == arcade.key.RIGHT:
            self.right = True
        if symbol == arcade.key.LEFT:
            self.left = True
        if symbol == arcade.key.UP:
            self.up = True
        if symbol == arcade.key.DOWN:
            self.down = True
        if symbol == arcade.key.D:
            self.s_right = True
        if symbol == arcade.key.A:
            self.s_left = True
        if symbol == arcade.key.W:
            self.s_up = True
        if symbol == arcade.key.S:
            self.s_down = True

    def on_key_release(self, symbol, modifiers):
        if symbol == arcade.key.RIGHT:
            self.right = False
        if symbol == arcade.key.LEFT:
            self.left = False
        if symbol == arcade.key.UP:
            self.up = False
        if symbol == arcade.key.DOWN:
            self.down = False
        if symbol == arcade.key.D:
            self.s_right = False
        if symbol == arcade.key.A:
            self.s_left = False
        if symbol == arcade.key.W:
            self.s_up = False
        if symbol == arcade.key.S:
            self.s_down = False

    def on_mouse_press(self, x: float, y: float, button: int, modifiers: int):
        if button == arcade.MOUSE_BUTTON_LEFT:
            self.player_x = x
            self.player_y = y

    def on_mouse_drag(self, x: float, y: float, dx: float, dy: float, buttons: int, modifiers: int):
        if buttons == arcade.MOUSE_BUTTON_LEFT:
            self.player_x = x
            self.player_y = y


window = arcade.Window(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE)
start_view = InstructionView()
window.show_view(start_view)
# start_view.setup()
arcade.run()
