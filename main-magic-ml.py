"""
CERN Webfest 2020 Machine Learning: How does it work?
"""

import arcade
from button import *

SCREEN_WIDTH = 1920
SCREEN_HEIGHT = 1080
SCREEN_TITLE = "Discovering the magic of Machine Learning"
SPRITES_URI = "resources/images/"
MUSIC_VOLUME = 0.01

class MyGame(arcade.View):
    """
    Main application class
    """
    def __init__(self, playsong):
        # Call the parent class and set up the window
        super().__init__()
        self.button_list = None

        # background
        self.background = arcade.load_texture(SPRITES_URI + "cern-background4-blur.jpg")
        # arcade.set_background_color(arcade.csscolor.CORNFLOWER_BLUE)

        # music
        if playsong:
            self.music_song = arcade.Sound("resources/music/1918.mp3", streaming=True)
            self.music_song.play(volume=MUSIC_VOLUME)




    def on_show(self):
        """ Set up the game here. Call this function to restart the game. """
        self.button_list = []
        mdbl_button = TextButton((SCREEN_WIDTH//4)-200, 570, 280, 600, "MORE DATA,\nBETTER LEARNER", self.loadViewMDBL)
        toa_button = TextButton((SCREEN_WIDTH//4-200)*2+200, 570, 280, 600, "TYPES OF\n ALGORITHMS", self.loadViewTOA)
        extra_button1 = TextButton((SCREEN_WIDTH // 4 - 200) * 3 + 350, 570, 280, 600, "...", self.do_nothing)
        extra_button2 = TextButton((SCREEN_WIDTH // 4 - 200) * 4 + 500, 570, 280, 600, "...", self.do_nothing)
        self.button_list.append(mdbl_button)
        self.button_list.append(toa_button)
        self.button_list.append(extra_button1)
        self.button_list.append(extra_button2)

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        # Code to draw the screen goes here
        arcade.draw_lrwh_rectangle_textured(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, self.background)
        for b in self.button_list:
            b.draw()




    def on_update(self, delta_time):
        """ Collisions and moving parts are checked here. """

    def on_mouse_press(self, x, y, button, key_modifiers):
        """
        Called when the user presses a mouse button.
        """
        check_mouse_press_for_buttons(x, y, self.button_list)

    def on_mouse_release(self, x, y, button, key_modifiers):
        """
        Called when a user releases a mouse button.
        """
        check_mouse_release_for_buttons(x, y, self.button_list)

    def loadViewMDBL(self):
        game_view = MoreDataBetterLearner()
        self.window.show_view(game_view)

    def loadViewTOA(self):
        game_view = TypesOfAlgorithms()
        self.window.show_view(game_view)

    def do_nothing(self):
        pass


class MoreDataBetterLearner(arcade.View):
    """
    Main application class
    """

    def on_show(self):
        # background
        self.background = arcade.load_texture(SPRITES_URI + "cern-background2-blur.jpg")
        # sprites
        self.sprite_list = arcade.SpriteList()
        self.sprite_webfest = arcade.Sprite(SPRITES_URI + "Webfest_online_logo.png", center_x=80, center_y=SCREEN_HEIGHT - 65)
        self.sprite_cern = arcade.Sprite(SPRITES_URI + "logo-outline-white.png", center_x=SCREEN_WIDTH-110, center_y=SCREEN_HEIGHT - 110)
        self.sprite_apprentice = arcade.Sprite(SPRITES_URI + "ml-sprite-big.png", center_x=SCREEN_WIDTH//2+400, center_y=SCREEN_HEIGHT//2)
        self.btn_plus = arcade.Sprite(SPRITES_URI + "btn_plus.png", scale=0.4, center_x=170, center_y=320)
        self.btn_neg = arcade.Sprite(SPRITES_URI + "btn_neg.png", scale=0.4, center_x=350, center_y=320)
        self.sprite_gear = arcade.Sprite(SPRITES_URI + "sprite-gear.png", center_x=SCREEN_WIDTH//2-200, center_y=SCREEN_HEIGHT//2)
        self.arrow_right = arcade.Sprite(SPRITES_URI + "arrow_right.png", center_x=SCREEN_WIDTH//2-520, center_y=SCREEN_HEIGHT//2)
        self.arrow_right2 = arcade.Sprite(SPRITES_URI + "arrow_right.png", center_x=SCREEN_WIDTH // 2 + 85, center_y=SCREEN_HEIGHT // 2)
        self.piled_books = arcade.Sprite(SPRITES_URI + "sprite-pile-books.png", center_x=SCREEN_WIDTH//2-750, center_y=SCREEN_HEIGHT//2)
        self.interrogationmark1 = arcade.Sprite(SPRITES_URI + "sprite_interrogationmark.png", scale= 0.6, center_x=SCREEN_WIDTH - 250, center_y=SCREEN_HEIGHT // 2 + 200)
        self.interrogationmark2 = arcade.Sprite(SPRITES_URI + "sprite_interrogationmark.png",
                                                scale= 0.6, center_x=SCREEN_WIDTH - 250, center_y=SCREEN_HEIGHT // 2 )
        self.interrogationmark3 = arcade.Sprite(SPRITES_URI + "sprite_interrogationmark.png",
                                                scale= 0.6, center_x=SCREEN_WIDTH - 250, center_y=SCREEN_HEIGHT // 2 -200)
        self.sprite_list.append(self.sprite_webfest)
        self.sprite_list.append(self.sprite_cern)
        self.sprite_list.append(self.sprite_apprentice)
        self.sprite_list.append(self.btn_plus)
        self.sprite_list.append(self.btn_neg)
        self.sprite_list.append(self.sprite_gear)
        self.sprite_list.append(self.arrow_right)
        self.sprite_list.append(self.arrow_right2)
        self.sprite_list.append(self.piled_books)
        self.sprite_list.append(self.interrogationmark1)
        self.sprite_list.append(self.interrogationmark2)
        self.sprite_list.append(self.interrogationmark3)

    def on_draw(self):
        arcade.start_render()
        arcade.draw_lrwh_rectangle_textured(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, self.background)
        # arcade.draw_text("Instructions Screen", SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2, arcade.color.WHITE, font_size=50, anchor_x="center")
        # arcade.draw_text("Click to advance", SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2 - 75, arcade.color.WHITE, font_size=20, anchor_x="center")
        self.sprite_list.draw()

    def on_mouse_press(self, _x, _y, _button, _modifiers):
        """ If the user presses the mouse button, start the game. """
        #game_view = MyGame()
        #self.window.show_view(game_view)

    def on_key_press(self, symbol, modifiers):
        if symbol == arcade.key.SPACE:
            self.load_main_view()

    def load_main_view(self):
        game_view = MyGame(False)
        self.window.show_view(game_view)


class TypesOfAlgorithms(arcade.View):
    """
    Main application class
    """

    def on_show(self):
        # background
        self.background = arcade.load_texture(SPRITES_URI + "cern-background1-blur.jpg")

    def on_draw(self):
        arcade.start_render()
        arcade.draw_lrwh_rectangle_textured(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, self.background)
        arcade.draw_text("TYPES OF ALGORITHM", SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2,
                         arcade.color.WHITE, font_size=50, anchor_x="center")
        arcade.draw_text("W.I.P", SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2 - 75,
                         arcade.color.WHITE, font_size=20, anchor_x="center")

    def on_mouse_press(self, _x, _y, _button, _modifiers):
        """ If the user presses the mouse button, start the game. """
        #game_view = MyGame()
        #self.window.show_view(game_view)

    def on_key_press(self, symbol, modifiers):
        if symbol == arcade.key.SPACE:
            self.load_main_view()

    def load_main_view(self):
        game_view = MyGame(False)
        self.window.show_view(game_view)


def main():
    """ Main method """
    window = arcade.Window(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE)
    start_view = MyGame(True)
    window.show_view(start_view)
    arcade.run()

    """ What it was before:
    window = MyGame()
    window.setup()
    arcade.run()
    """

if __name__ == "__main__":
    main()